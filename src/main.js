import Vue from "vue";
import Vuetify from "vuetify";
import App from "./App.vue";

import 'material-design-icons-iconfont/dist/material-design-icons.css';
import 'vuetify/dist/vuetify.min.css';
var _ = require('lodash');

window.axios = require('axios');

Vue.config.productionTip = false;
Vue.use(Vuetify);
new Vue({
  render: h => h(App)
}).$mount("#app");
